# MockServer

Ready to use docker image of: https://www.npmjs.com/package/mockserver

## Usage

On your `docker-compose.yml`:

```yaml
version: "3.7"

services:
  my-fake-api:
    image: registry.gitlab.com/nexylan/docker/mockserver:x.y.z
    volumes:
      - "./your/mocks/path:/mocks"
```

To learn how to configure mockserver and create your mocks file, please refer to the
[official documentation](https://github.com/namshi/mockserver#mock-files).
