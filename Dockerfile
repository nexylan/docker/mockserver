FROM node:14-alpine
RUN npm install --global mockserver@3.1.1
CMD [ "mockserver" ,"--port", "80", "--mocks", "/mocks"]
